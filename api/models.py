from django.db import models


class List(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)


class Task(models.Model):
    text = models.TextField(blank=False, null=False)
    complete = models.BooleanField(default=False)
    list = models.ForeignKey(List, on_delete=models.CASCADE)




