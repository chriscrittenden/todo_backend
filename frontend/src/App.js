import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import DeleteIcon from '@material-ui/icons/Delete';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListIcon from '@material-ui/icons/List';
import AddIcon from '@material-ui/icons/Add';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import produce from 'immer';
import {Route, Link} from 'react-router-dom';
import axios from 'axios';

const base_url = "";

export class SignIn extends Component {
    render() {
        return (
            <div style={{flexGrow: 1}}>
                <CssBaseline />
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="title" color="inherit" style={{flexGrow: 1}}>
                            Sign In
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div style={{display: 'flex', justifyContent: 'center', marginTop: '40px'}}>
                    <Paper style={{width: '100%', maxWidth: '400px'}}>
                        <div>
                            <ListItem>
                                <ListItemText primary={<TextField placeholder="Username" style={{width: '100%'}} />} />
                            </ListItem>
                            <ListItem>
                                <ListItemText primary={<TextField placeholder="Password" style={{width: '100%'}} />} />
                            </ListItem>
                            <ListItem>
                                <Button color='primary' component={Link} to={'/tasks'}>Sign In</Button>
                            </ListItem>
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
}

export class App extends Component {
    state = {
        drawer_open: false,
        add_list_open: false,
        lists: [],
        new_list: "",
    };

    componentDidMount() {
        axios.get(base_url + "/lists/").then(res => {
                this.setState({'lists': res.data});
            }
        )
    };

    handleAddList = () => {
        if (this.state.new_list !== "") {
            axios.post(base_url + "/lists/", {'name': this.state.new_list}).then(res => {
                this.setState({
                    lists: produce(this.state.lists, draft => {draft.push(res.data)}),
                    new_list: "",
                    add_list_open: false
                });
            });
        }
    };

    handleDeleteList = (list_id) => {
        axios.delete(base_url + "/lists/" + list_id + "/").then(res => {
            this.props.history.push('/tasks/');
            this.setState({
                lists: res.data
            });
        });
    };

    render() {
        return (
            <div style={{flexGrow: 1}}>
                <CssBaseline />

                {/* The AppBar */}
                <AppBar position="static">
                    <Toolbar>
                        <IconButton style={{marginLeft: -12, marginRight: 20}} color="inherit" onClick={() => this.setState({drawer_open: true})}>
                            <MenuIcon />
                        </IconButton>
                        <Route path={'/tasks/list/:id'} render={(routeProps) => {
                            return (
                                <Typography variant="title" color="inherit" style={{flexGrow: 1}}>{this.state.lists.length > 0 && this.state.lists.find(list => list.id === parseInt(routeProps.match.params.id, 10)).name}</Typography>
                            )}}
                        />
                        <Route exact path={'/tasks/'} render={(routeProps) => <Typography variant="title" color="inherit" style={{flexGrow: 1}}>To Do List</Typography>}/>
                        <Button color="inherit" component={Link} to={'/'}>Sign Out</Button>
                    </Toolbar>
                </AppBar>

                {/* The Drawer */}
                <Drawer open={this.state.drawer_open} onClose={() => this.setState({drawer_open: false})}>
                    <div>
                        {this.state.lists.map(list => {
                            return (
                                <ListItem key={list.id} button onClick={() => this.setState({drawer_open: false})} component={Link} to={'/tasks/list/' + list.id}>
                                    <ListItemIcon>
                                        <ListIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={list.name} />
                                </ListItem>
                            )
                        })}
                        <ListItem button onClick={() => this.setState({add_list_open: true})}>
                            <ListItemIcon>
                                <AddIcon />
                            </ListItemIcon>
                            <ListItemText primary="Create List" />
                        </ListItem>
                    </div>
                </Drawer>

                {/* The List */}
                <Route path={'/tasks/list/:list_id'} render={routeProps => {
                    return (
                        <List {...routeProps} lists={this.state.lists} handleDeleteList={this.handleDeleteList}/>
                    )
                }}/>

                {/*The Add List Dialog*/}
                <Dialog open={this.state.add_list_open} onClose={() => this.setState({add_list_open: false})}>
                    <DialogTitle>Create a new list</DialogTitle>
                    <DialogContent>
                        <ListItem>
                            <ListItemText primary={<TextField placeholder="Name your list" style={{width: '100%'}} value={this.state.new_list} onChange={(e) => this.setState({new_list: e.target.value})} />} />
                            <ListItemIcon>
                                <IconButton onClick={this.handleAddList}><AddIcon/></IconButton>
                            </ListItemIcon>
                        </ListItem>
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}

class List extends Component {
    state = {
        new_task: "",
        tasks: []
    };

    componentDidMount() {
        const list_id = this.props.match.params.list_id;
        this.fetch(list_id);
    }

    componentWillReceiveProps(nextProps) {
        const next_list_id = nextProps.match.params.list_id;
        if (this.props.match.params.list_id !== next_list_id) {
            this.fetch(next_list_id);
        }
    }

    fetch = (list_id) =>  {
        axios.get(base_url + "/lists/" + list_id + "/tasks/").then(res => {
            this.setState({
                tasks: res.data
            });
        });
    };

    handleAddTask = (list_id) => {
        if (this.state.new_task !== "") {
            axios.post(base_url + "/lists/" + list_id + "/tasks/", {'list': list_id, 'text': this.state.new_task, 'complete': false}).then(res => {
                this.setState({
                    tasks: produce(this.state.tasks, draft => {draft.push(res.data)}),
                    new_task: ""
                });
            });
        }
    };

    handleCheck = (list_id, task_id, checked) => {
        const task_index = this.state.tasks.findIndex(task => task.id === task_id);
        const task = this.state.tasks[task_index];

        axios.put(base_url + "/lists/" + list_id + "/tasks/" + task_id + "/", {'id': task_id, 'list': list_id, 'text': task.text, 'complete': checked}).then(res => {
            this.setState({
                tasks: produce(this.state.tasks, draft => {draft[task_index] = res.data})
            })
        });
    };

    handleDeleteTask = (list_id, task_id) => {
        axios.delete(base_url + "/lists/" + list_id + "/tasks/" + task_id + "/").then(res => {
            this.setState({
                tasks: res.data
            })
        });
    };

    render () {
        const list_id = parseInt(this.props.match.params.list_id, 10);

        return (
            <div style={{display: 'flex', justifyContent: 'center'}}>
            <Paper style={{maxWidth: '1020px', width: '100%', marginTop: '40px'}}>
                <div>
                    <div style={{display: 'flex', padding: '24px 24px'}}>
                        <Typography variant="headline" style={{flexGrow: 1}}>{this.props.lists.length > 0 && this.props.lists.find(list => list.id === list_id).name}</Typography>
                        <Button color="secondary" variant="raised" size="small" onClick={() => this.props.handleDeleteList(list_id)}>Delete List</Button>
                    </div>
                    <ListItem>
                        <ListItemText primary={<TextField placeholder="Type a new task" style={{width: '100%'}} value={this.state.new_task} onChange={(e) => this.setState({new_task: e.target.value})}/>} />
                        <ListItemIcon><IconButton onClick={() => this.handleAddTask(list_id)}><AddIcon/></IconButton></ListItemIcon>
                    </ListItem>
                    {this.state.tasks.map(task => {
                        return (
                            <ListItem key={task.id}>
                                <ListItemIcon>
                                    <Checkbox
                                        checked={task.complete}
                                        onChange={(e, checked) => this.handleCheck(list_id, task.id, checked)}
                                    />
                                </ListItemIcon>
                                <ListItemText primary={task.text}/>
                                <IconButton onClick={() => this.handleDeleteTask(list_id, task.id)}><DeleteIcon/></IconButton>
                            </ListItem>
                        )
                    })}
                </div>
            </Paper>
        </div>
        )
    }
}