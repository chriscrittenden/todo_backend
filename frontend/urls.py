from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import TemplateView
from api import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lists/', views.list_list),
    path('lists/<list_id>/', views.list_detail),
    path('lists/<list_id>/tasks/', views.task_list),
    path('lists/<list_id>/tasks/<task_id>/', views.task_detail),
    re_path('.*', TemplateView.as_view(template_name='index.html')),
]

urlpatterns = format_suffix_patterns(urlpatterns)

